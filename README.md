# SMS Send Script

This script is/was only intended for my personal use to allow me to send SMS from command line
cheap and easily. So instead of using something like Twilio it relies on `Servers Ultimate` App for Android.

The script is split into two parts:
* `sms` can be called with `sms [number] [body]` to send a sms and will send a request to `relay.php`
* `relay.php` forwards the request from `sms` to the Android Phone running `Servers Ultimate`  

## Installation
Download files and put sms in some folder in your `$PATH`. 

Put `relay.php` on a PHP Server in your network. 

On your Android Device download `Servers Ultimate`, create and start a SMS Gateway Server.

https://play.google.com/store/apps/details?id=com.icecoldapps.serversultimatepro

You can try the free version but as far as I know the free version is limited to 7 days. I once got it very cheap from 
the Amazon App Store so you might want to check the price there, too.

### Configure SMS
At the top of the `sms` file you'll find some constants you need to change:
* `ME` put in your own number, so you can use the shortcut `sms me [body]` to send a sms to your phone.
* `Protocol` http or if your PHP Server supports it use https.
* `Server` IP/Hostname of your Server running your `relay.php` script.
* `Port` Port under which your Server is accessable.
* `Path` Path to your `relay.php` script. Must contain `relay.php` or whatever you call that file.
* `Password` Password you defined in `relay.php`.

At the top of the `relay.php` file you'll find some more constants you need to change:
* `Password` Set a password which is needed in order to use `relay.php`.
* `Protocol` Most likely `http://`. This is the protocol used by your `Servers Ultimate App`.
* `Server` IP/Hostname of your Android Device running the app.
* `Port` Port under which your SMS Gateway Server is accessable.
* `Path` Most likely `send.html`.

## Dependencies
Relies on `PHP`.
 
## Usage
`sms me "Hi there!"` will send the message to the `me` number defined in the `sms` file.

`sms 01710000000 "Hi there!"`

## Tips and things I'd like to mention
The password you configure in the files are only for access to `relay.php`. People on your network can still access the
Webinterface of the SMS Gateway Server and send SMS. So you might limit access to your Gateway Server.

If you're asking yourself why I use a php relay instead of sending the request directly to the SMS Gateway here is the 
reason: I think this is more expandable for the future because one day there might be a better SMS Gateway App or `Servers Ultimate` 
gets discontinued. Who knows. And it also allows you to easily block users from sending SMS.

## Licence
Apache 2, see `LICENCE`

If you need another licence, contact me.