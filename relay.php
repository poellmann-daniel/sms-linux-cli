<?php

const Password = "1o3B8acNhEV4";

const Protocol = "http://";
const Server = "192.168.1.XXX";
const Port = "8585";
const Path = "send.html";


if (isset($_GET['pass'])) {
    $pass = $_GET['pass'];
} else {
    echo "error: password not available";
    exit();
}

if (isset($_GET['body'])) {
    $body = $_GET['body'];



} else {
    echo "error: body not available";
    exit();
}

if (isset($_GET['number'])) {
    $number = $_GET['number'];
} else {
    echo "error: number not available";
    exit();
}

if ($pass == Password) {
    $data = array(
        'smsto'=>$number,
        'smsbody'=>$body,
        'smstype'=>'sms');

    $url_parameter = http_build_query($data);

    $return = file_get_contents(Protocol . Server . ":" . Port . "/" . Path . "?" . $url_parameter);

    if (strpos($return, 'The SMS has been sent!') !== false) {
        echo "success: message was delivered to the gateway";
        exit();
    }

} else {
    echo "error: password not valid";
    exit();
}